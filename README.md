# KZA Connected

KZA Connected kan in zn geheel worden opgespint met docker.  
Voer daarvoor achtereenvolgens onderstaande commandos uit.  
Wacht tussentijds even totdat de container volledig is opgestart.  
(dit laatste kun je checken door ```docker ps -a``` gevolgd door ```docker logs <container id>```)  

1. Draai de database dmv ```docker-compose up -d cursus_database```.  
   De database draait op localhost:3306.  
   Er wordt ook een volume mount aangemaakt waar de data naartoe wordt weggeschreven.  
   Dit is een lokale map met pad ```c:/db```.  
2. Draai de service dmv ```docker-compose up -d cursus_service```.  
   De service draait op localhost:8080.  
   Service endpoints zijn direct te benaderen of middels een Swagger client, te vinden onder: localhost:8080/swagger-ui.html  
3. Draai de client dmv ```docker-compose up -d cursus_client```.  
   De client draait op localhost:4200.  